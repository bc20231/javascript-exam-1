import { firstGrownUp, firstOrange, firstLengthOver5Name } from "../src/find";

describe("array find test", () => {
  // Please add test cases here
  test("filter: should return 23", () => {
    const ages = [12, 8, 23, 18, 27];
    const result = firstGrownUp(ages);

    expect(result).toBe(23);
  });
  
  test("filter: should return orange", () => {
    const words = ['apple', 'lime', 'orange', 'pear'];
    const result = firstOrange(words);

    expect(result).toBe('orange');
  });
  
  test("filter: should return first name with more than 5 letters", () => {
    const names = ['Emily', 'Alice', 'Victoria', 'Yvonne'];
    const result = firstLengthOver5Name(names);

    expect(result).toBe('Victoria');
  });
});
