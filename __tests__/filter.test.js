import { filterEvenNumbers, filterLengthWith4, filterStartWithA } from '../src/filter';

describe('array filter test', () => {
    // Please add test cases here
	test("filter: should return even numbers", () => {
    const numbers = [1, 2, 3, 4, 5, 6];
    const result = filterEvenNumbers(numbers);

    expect(result).toEqual([2, 4, 6]);
  });
  
  test("filter: should return words with 4 letters", () => {
    const words = ['apple', 'lime', 'orange', 'pear'];
    const result = filterLengthWith4(words);

    expect(result).toEqual(['lime', 'pear']);
  });
  
  test("filter: should return words start with A", () => {
    const words = ['apple', 'lime', 'avocado', 'pear'];
    const result = filterStartWithA(words);

    expect(result).toEqual(['apple', 'avocado']);
  });
});