import { addSerialNumber, halfNumbers, spliceNames } from "../src/map";

describe("array map test", () => {
  // Please add test cases here
  test("filter: should return all nums divided by 2", () => {
    const numbers = [12, 10, 34, 26, 52];
    const result = halfNumbers(numbers);

    expect(result).toEqual([6, 5, 17, 13, 26]);
  });
  
  test("filter: should return Hello followed by each name", () => {
    const names = ['Emily', 'Alice', 'Victoria'];
    const result = spliceNames(names);

    expect(result).toEqual(['Hello Emily', 'Hello Alice', 'Hello Victoria']);
  });
  
  test("filter: should return index followed by each fruit", () => {
    const fruits = ['apple', 'lime', 'orange'];
    const result = addSerialNumber(fruits);

    expect(result).toEqual(['1. apple', '2. lime', '3. orange']);
  });
});
